package kadai
package config

import result.{ Result, ResultT }
import result.syntax._

import scalaz.{ Applicative, Kleisli, MonadTrans, \/ }
import scalaz.syntax.all._
import scalaz.std.option._
import scalaz.syntax.std.option._

trait ConfigTypes {

  type ConfigResult[A] = ResultT[ConfigReader, A]

  object ConfigResult {
    def apply[A](f: Configuration => Result[A]): ConfigResult[A] =
      ResultT { ConfigReader(f andThen { _.run }) }

    def lift[A](c: ConfigReader[A]): ConfigResult[A] =
      MonadTrans[ResultT].liftM { c }

    def ask: ConfigResult[Configuration] =
      ResultT.right[ConfigReader, Configuration] { Kleisli.ask }

    def get[A: Configuration.Accessor](name: String): ConfigResult[A] =
      apply { _[Result[A]](name) }

    def option[A: Configuration.Accessor](name: String): ConfigResult[Option[A]] =
      safe { _[Option[A]](name) }

    def keys(name: String): ConfigResult[List[String]] =
      safe { _.keys(name).toList }

    def safe[A](f: Configuration => A): ConfigResult[A] =
      apply { c => Result.catchingToResult { f(c) } }

    def catching[A](f: => A): ConfigResult[A] =
      ResultT.catching { f }

    /** ConfigResult that reads from a modified Configuration. */
    def local[A](mod: Configuration => Configuration)(r: ConfigResult[A]): ConfigResult[A] =
      ResultT[ConfigReader, A] { r.run.local(mod) }

    /** local ConfigResult that reads from the subsection without further qualifiers. */
    def sub[A](section: String)(r: ConfigResult[A]): ConfigResult[A] =
      local { _.get[Configuration](section) }(r)

    /** local ConfigResult that returns none if the subsection doesn't exist */
    def subOption[A](section: String)(r: ConfigResult[A]): ConfigResult[Option[A]] =
      for {
        oc <- ConfigResult.option[Configuration](section)
        cr <- oc.some { c => r.sub(section).liftOption }.none { ConfigResult.catching { none } }
      } yield cr

    implicit class syntax[A](self: ConfigResult[A]) {
      import ConfigReader._

      /** run the ConfigResult and get the required ResultT out of it */
      def extract[F[_]: Applicative](c: Configuration): ResultT[F, A] =
        ResultT {
          Attempt.safe { self.run.extract(c) }.fold(_.left[A], identity).point[F]
        }

      /** run the ConfigResult and get a disjunction out of it, note that this doesn't catch */
      def extractId(c: Configuration): Invalid \/ A =
        Result.catchingToResult { self.run.extract(c) }.run.fold(_.left[A], identity)

      def local(mod: Configuration => Configuration): ConfigResult[A] =
        ConfigResult.local(mod)(self)

      /** local ConfigResult that reads from the subsection without further qualifiers. */
      def sub(section: String): ConfigResult[A] =
        ConfigResult.sub(section)(self)

      /** local ConfigResult that returns none if the subsection doesn't exist */
      def subOption(section: String): ConfigResult[Option[A]] =
        ConfigResult.subOption(section)(self)

      def liftOption: ConfigResult[Option[A]] =
        lift { self.fold(_ => none, some) }
    }

    implicit class readerSyntax[A](r: ConfigReader[A]) {
      def toConfigResult: ConfigResult[A] =
        lift { r }
    }

    implicit class failedConfigMessageSyntax(m: String) {
      def configError[A]: ConfigResult[A] =
        ConfigResult { _ => m.failureResult }
    }
  }
}

object ConfigTypes extends ConfigTypes