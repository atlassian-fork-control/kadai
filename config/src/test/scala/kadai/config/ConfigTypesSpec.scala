package kadai
package config

import com.typesafe.config.{ConfigFactory, ConfigResolveOptions}
import org.specs2.SpecificationWithJUnit

import scalaz.{\/, \/-}
import scalaz.Scalaz.Id

class ConfigTypesSpec extends SpecificationWithJUnit with org.specs2.matcher.DisjunctionMatchers {
  import ConfigResult._

  def is =
    s2"""
         This specification tests ConfigResult combinators

         sub works                                  $subWorks
         sub works in for comprehensions            $subsections
         sub works with embedded ConfigResults      $subsections2
         sub fails if embedded ConfigResult fails   $failingSubsection
         option returns Some if present             $optionWorks
         option returns none if not present         $optionFails
         get[Option] returns Some if present        $getOptionWorks
         get[Option] returns none if not present    $getOptionFails
         liftOption returns Some if present         $liftOptionWorks
         liftOption returns none if not present     $liftOptionFails
         subOption returns Some if present          $subOptionWorks
         subOption returns none if not present      $subOptionWorks
    """

  def subWorks = {
    val cfg =
      Configuration.from {
        """|my-val = 1
         |bar {
         |  my-val = 2
         |}""".stripMargin
      }
    val c: ConfigResult[Int] = ConfigReader.named[Int]("my-val").toConfigResult

    run(c, cfg) must be_\/-(1) and { run(c.sub("bar"), cfg) must be_\/-(2) } and { run(c.sub("foo"), cfg) must be_-\/ }
  }

  import ConfigResult._

  def subsections =
    run {
      for {
        s <- sub("config.embed.more") { get[String]("string") }
        t <- sub("config.embed.more") { get[String]("thevar") }
      } yield (s, t)
    } must be_\/-("woohoo" -> "something to substitute")

  def subsections2 =
    run {
      for {
        a <- sub("config.embed.more") {
          for {
            s <- get[String]("string")
            t <- get[String]("thevar")
          } yield s -> t
        }
      } yield a
    } must be_\/-("woohoo" -> "something to substitute")

  def failingSubsection =
    run {
      for {
        a <- sub("config.embed.more") {
          for {
            s <- get[String]("frooble")
            t <- get[String]("thevar")
          } yield s -> t
        }
      } yield a
    } must be_-\/

  def optionWorks =
    run { option[Int]("thing.id") } must be_\/- { Some(667) }

  def optionFails =
    run { option[String]("frooble") } must be_\/-(None)

  def getOptionWorks =
    run { get[Option[Int]]("thing.id") } must be_\/- { Some(667) }

  def getOptionFails =
    run { get[Option[String]]("frooble") } must be_\/-(None)

  def listKeys =
    run { ConfigResult.keys("thing") } must be_\/- { List("id", "someUrl") }

  def liftOptionWorks =
    run { get[Int]("thing.id").liftOption } must be_\/- { Some(667) }

  def liftOptionFails =
    run { get[Int]("frooble").liftOption } must be_\/-(None)

  def subOptionWorks =
    run { get[Int]("id").subOption("thing") } must be_\/- { Some(667) }

  def subOptionFails =
    run { get[Int]("id").subOption("frooble") } must be_\/- { None }

  private def run[A](c: ConfigResult[A], config: Configuration = testConfiguration): Invalid \/ A =
    c extractId config

  private def testConfiguration: Configuration =
    Configuration {
      ConfigFactory.defaultOverrides.withFallback {
        ConfigFactory.load("test.conf", Configuration.failIfMissing.setClassLoader(getClass.getClassLoader), ConfigResolveOptions.defaults)
      }
    }

}
